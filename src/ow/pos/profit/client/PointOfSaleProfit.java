package ow.pos.profit.client;

import java.util.ArrayList;
import java.util.List;

import ow.pos.profit.shared.ButtonStyles;
import ow.pos.profit.model.component.ButtonEnableEditTextCell;
import ow.pos.profit.model.component.CheckLogin;
import ow.pos.profit.model.component.DatePickerPopup;
import ow.pos.profit.shared.TableSort;
import ow.pos.profit.shared.HTTPGetString;
import ow.pos.profit.model.layout.Buyer;
import ow.pos.profit.model.layout.InvoiceDropDown;
import ow.pos.profit.model.structure.DropDownItem;
import ow.pos.profit.model.structure.Profit;
import ow.pos.profit.shared.ConfigParameters;

import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;

public class PointOfSaleProfit implements EntryPoint {
	
	private Label errorMsgLabel;
	
	private CellTable<Profit> profitTable;
	private ListDataProvider<Profit> dataProvider;
	
	private Buyer buyer = new Buyer();
	
	private HorizontalPanel mainPanel;
	
	private Label fromLabel = new Label("From:");
	private DatePickerPopup dateFrom = new DatePickerPopup();
	private Label toLabel = new Label("To:");
	private DatePickerPopup dateTo = new DatePickerPopup();
	private Button updateBtn = new Button("Update");
	
	private InvoiceDropDown supplierDropDown;
	private TableSort<Profit> tableSorter;
	private VerticalPanel contentWrapper = new VerticalPanel();
	private final int[] textCols = { 1 };
	
	private ButtonEnableEditTextCell editTextCell = new ButtonEnableEditTextCell();
	
	public void onModuleLoad(){
		new CheckLogin();
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.add(fromLabel);
		hp.add(dateFrom);
		hp.add(toLabel);
		hp.add(dateTo);
		hp.add(updateBtn);
		
		updateBtn.setStyleName(ButtonStyles.REGULARBUTTONCLASS, true);
		updateBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				HTTPGetString params = new HTTPGetString();
				params.addParam("startDate", dateFrom.getText());
				params.addParam("endDate", dateTo.getText());
				String requestData = params.toString();
				getSupplierList(requestData);
			}
		});
		
		
		mainPanel = new HorizontalPanel();
		
		supplierDropDown = new InvoiceDropDown();
		getSupplierList("");
		supplierDropDown.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				int index = supplierDropDown.getSelectedIndex();
				String Id = supplierDropDown.getValue(index);
				HTTPGetString params = new HTTPGetString();
				params.addParam("id", Id);
				params.addParam("startDate", dateFrom.getText());
				params.addParam("endDate", dateTo.getText());
				String requestData = params.toString();
				getProfitTable(requestData);
			}
		});
				
		mainPanel.add(supplierDropDown);
		
				
		SimplePanel accountTableContainer = new SimplePanel();
		loadItem();
		accountTableContainer.add(profitTable);
		contentWrapper.add(accountTableContainer);
		
		RootPanel.get().add(hp);
		RootPanel.get().add(mainPanel);
		RootPanel.get().add(contentWrapper);
	}
		
	private void loadItem(){
		
		profitTable = new CellTable<Profit>();
						
		TextColumn<Profit> InvoiceIdCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getInvoiceId();
			}
		};
		
		TextColumn<Profit> NameCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getName();
			}
		};
		
		Column<Profit, String> ConsignmentIdCol = new Column<Profit, String>(new ClickableTextCell()){
			public String getValue(Profit item) {
				return item.getConsignmentId();
			}
		};
		ConsignmentIdCol.setFieldUpdater(new FieldUpdater<Profit, String>(){
			public void update(int index, Profit object, String value){
				buyer.showReport(object.getId());
			}
		});
		
		TextColumn<Profit> DateCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getDate();
			}
		};
		
		TextColumn<Profit> QtyInCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getQtyIn();
			}
		};
		
		TextColumn<Profit> QtyOutCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getQtyOut();
			}
		};
		
		TextColumn<Profit> QtyGainCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getQtyGain();
			}
		};
		
		TextColumn<Profit> QtyLossCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getQtyLoss();
			}
		};
		
		TextColumn<Profit> QtyTotalCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getQtyTotal();
			}
		};
		
		TextColumn<Profit> RemainingCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getRemaining();
			}
		};
		
		TextColumn<Profit> TotalPriceCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getTotalPrice();
			}
		};
		
		TextColumn<Profit> AvgPriceCol = new TextColumn<Profit>() {
			public String getValue(Profit item) {
				return item.getAvgPrice();
			}
		};
		
		Column<Profit, String> PriceCol = new Column<Profit, String>(editTextCell) {
			public String getValue(Profit item) {
				return item.getPrice();
			}
		};
		PriceCol.setFieldUpdater(new FieldUpdater<Profit, String>(){
			public void update(int index, Profit object, String value){
				HTTPGetString request = new HTTPGetString();
				request.addParam("invoiceId", object.getInvoiceId());
				request.addParam("itemId", object.getId());
				request.addParam("isSupplier", "true");
				request.addParam("qty", object.getQtyIn());
				request.addParam("price", value);
				sendRequest("edititem", request.toString());
			}
		});
			
		profitTable.addColumn(InvoiceIdCol, "Invoice Id");
		profitTable.setColumnWidth(InvoiceIdCol, 60, Unit.PX);
		profitTable.addColumn(NameCol, "Name");
		profitTable.setColumnWidth(NameCol, 250, Unit.PX);
		profitTable.addColumn(DateCol, "Date");
		profitTable.setColumnWidth(DateCol, 60, Unit.PX);
		profitTable.addColumn(ConsignmentIdCol, "Con Id");
		profitTable.setColumnWidth(ConsignmentIdCol, 60, Unit.PX);
		profitTable.addColumn(QtyInCol, "Qty In");
		profitTable.setColumnWidth(QtyInCol, 60, Unit.PX);
		profitTable.addColumn(QtyOutCol, "Qty Out");
		profitTable.setColumnWidth(QtyOutCol, 60, Unit.PX);
		profitTable.addColumn(QtyGainCol, "Qty Gain");
		profitTable.setColumnWidth(QtyGainCol, 60, Unit.PX);
		profitTable.addColumn(QtyLossCol, "Qty Loss");
		profitTable.setColumnWidth(QtyLossCol, 60, Unit.PX);
		profitTable.addColumn(QtyTotalCol, "Qty Total");
		profitTable.setColumnWidth(QtyTotalCol, 60, Unit.PX);
		profitTable.addColumn(RemainingCol, "Remaining");
		profitTable.setColumnWidth(RemainingCol, 60, Unit.PX);
		profitTable.addColumn(TotalPriceCol, "Total");
		profitTable.setColumnWidth(TotalPriceCol, 60, Unit.PX);
		profitTable.addColumn(AvgPriceCol, "AvgPrice");
		profitTable.setColumnWidth(AvgPriceCol, 60, Unit.PX);
		profitTable.addColumn(PriceCol, "Price");
		profitTable.setColumnWidth(PriceCol, 60, Unit.PX);
		
		NameCol.setSortable(true);
		DateCol.setSortable(true);
				
		dataProvider = new ListDataProvider<Profit>();
		dataProvider.addDataDisplay(profitTable);
				
		tableSorter = new TableSort<Profit>(profitTable, dataProvider);
				
	}
	
	private void getProfitTable(String requestData){
		
		String url = ConfigParameters.BASEURL + "/profitjson/getProfitLoss?"
			+ requestData;
		
		JsonpRequestBuilder builder = new JsonpRequestBuilder();
		
		builder.requestObject(url, new AsyncCallback<JsArray<Profit>>() {
			@Override
			public void onFailure(Throwable caught) {
				displayError(caught.getMessage());
			}

			@Override
			public void onSuccess(JsArray<Profit> result) {
				List<Profit> itmList = new ArrayList<Profit>();

				if (result == null) {
					clearStatement();
				} else {
					profitTable.setRowCount(result.length());
					profitTable.setPageSize(result.length());
					
					for(int i = 0; i < result.length(); i++){
						itmList.add(result.get(i));
					}

				}

				dataProvider.setList(itmList);
				tableSorter.attachSort(textCols, false);
				tableSorter.attachDateSort(2, false);
				editTextCell.setClick(true);
											
			}
		});
					
	}
		
	private void sendRequest(String functionName, String requestData) {
		String url = ConfigParameters.BASEURL + "/jsondataserver/"
				+ functionName + "?" + requestData;

		JsonpRequestBuilder builder = new JsonpRequestBuilder();

		// send request with null data
		builder.requestString(url, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				displayError(caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				
			}
		});
	}
	
	private void getSupplierList(String requestData){
		
		setSupplierList(supplierDropDown, "", requestData);
	}
		
	private void setSupplierList(
			final InvoiceDropDown list, final String defaultVal, String requestData){
		
		String url = ConfigParameters.BASEURL + "/profitjson/getSupplierList?"
			+ requestData;
		
		JsonpRequestBuilder builder = new JsonpRequestBuilder();
		// send request with null data
		builder.requestObject(url, new AsyncCallback<JsArray<DropDownItem>>() {
			@Override
			public void onFailure(Throwable caught) {
				displayError(caught.getMessage());
			}

			@Override
			public void onSuccess(JsArray<DropDownItem> result) {
				list.setValues(result);
				list.setSelectedValue(defaultVal);
			}
		});
		
	}
	
	private void clearStatement(){
		List<Profit> itmList = new ArrayList<Profit>();
		dataProvider.setList(itmList);
	}
	
	private void displayError(String err) {
		errorMsgLabel.setText("Error: " + err);
		errorMsgLabel.setVisible(true);
	}
			
}
