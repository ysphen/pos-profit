package ow.pos.profit.model.structure;

import com.google.gwt.core.client.JavaScriptObject;

public class JSONDataWrapper extends JavaScriptObject {

	public final native String getData() /*-{ return this.data; }-*/;
	
	protected JSONDataWrapper() {
		
	}
}
