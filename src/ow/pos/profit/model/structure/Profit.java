package ow.pos.profit.model.structure;

import com.google.gwt.core.client.JavaScriptObject;

public class Profit extends JavaScriptObject{
	public final native String getId() /*-{
		return this.item_id;
	}-*/;
	public final native String getInvoiceId() /*-{
		return this.invoice_id;
	}-*/;
	public final native String getName() /*-{
		return this.name;
	}-*/;
	public final native String getConsignmentId() /*-{
		return this.consignment_id;
	}-*/;
	public final native String getDate() /*-{
		return this.date;
	}-*/;
	public final native String getQtyIn() /*-{
		return this.qty_in;
	}-*/;
	public final native String getQtyOut() /*-{
		return this.qty_out;
	}-*/;
	public final native String getQtyGain() /*-{
		return this.qty_gain;
	}-*/;
	public final native String getQtyLoss() /*-{
		return this.qty_loss;
	}-*/;
	public final native String getTotalPrice() /*-{
		return this.total_price;
	}-*/;
	public final native String getQtyTotal() /*-{
		return this.qty_total;
	}-*/;
	public final native String getRemaining() /*-{
		return this.remaining;
	}-*/;
	public final native String getAvgPrice() /*-{
		return this.avg_price;
	}-*/;
	public final native String getPrice() /*-{
		return this.price;
	}-*/;
	public final native String getSupplierId() /*-{
		return this.supplier_id;
	}-*/;
	protected Profit(){
		
	}
}
