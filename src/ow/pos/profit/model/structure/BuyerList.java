package ow.pos.profit.model.structure;

import com.google.gwt.core.client.JavaScriptObject;

public class BuyerList extends JavaScriptObject{
	public final native String getId() /*-{
		return this.id;
	}-*/;
	
	public final native String getName() /*-{
		return this.name;
	}-*/;
	
	public final native String getQty() /*-{
		return this.qty;
	}-*/;
	
	public final native String getPrice() /*-{
		return this.price;
	}-*/;
	
	public final native String getTotal() /*-{
		return this.total;
	}-*/;
	
	protected BuyerList(){
		
	}
}
