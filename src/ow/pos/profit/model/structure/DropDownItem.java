package ow.pos.profit.model.structure;

import com.google.gwt.core.client.JavaScriptObject;

public class DropDownItem extends JavaScriptObject {
	public final native String getValue() /*-{
		return this.value;
	}-*/;

	public final native String getDisplayText() /*-{
		return this.display;
	}-*/;

	protected DropDownItem() {

	}
}
