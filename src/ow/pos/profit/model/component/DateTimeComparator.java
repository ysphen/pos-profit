package ow.pos.profit.model.component;

import java.util.Comparator;
import java.util.Date;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;

public class DateTimeComparator<T extends JavaScriptObject> implements Comparator<T> {
	private TextColumn<T> column;
	
	public DateTimeComparator(TextColumn<T> col) {
		this.column = col;
		
	}

	@Override
	public int compare(T o1, T o2) {
		if (o1 == o2) {
			return 0;
		}

		if (o1 != null) {
			if (o2 != null) {
				Date o1Date = DateTimeFormat.getFormat("dd-MM-yyyy HH:mm:ss")
						.parse(column.getValue(o1));
				Date o2Date = DateTimeFormat.getFormat("dd-MM-yyyy HH:mm:ss")
						.parse(column.getValue(o2));

				return o1Date.compareTo(o2Date);
			} else {
				return -1;
			}
		}

		return -1;
	}
}
