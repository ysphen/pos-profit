package ow.pos.profit.model.component;

import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;

public class ButtonEnableEditTextCell extends EditTextCell {

    private boolean click;

    @Override
    public void onBrowserEvent(Context context, Element parent, String value, NativeEvent event,
            ValueUpdater<String> valueUpdater) {
        if (click) {
            super.onBrowserEvent(context, parent, value, event, valueUpdater);
        }
    }

    public void setClick(Boolean click) {
        this.click = click;
    }
}