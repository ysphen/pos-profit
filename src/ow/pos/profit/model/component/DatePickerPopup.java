package ow.pos.profit.model.component;

import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DatePicker;

public class DatePickerPopup extends TextBox {
	private final String DATEPICKERCLASS = "datePicker";
	private DatePicker picker;
	private PopupPanel pickerPopup;

	public DatePickerPopup() {
		this.setStyleName(DATEPICKERCLASS, true);
		pickerPopup = new PopupPanel(true);
		picker = new DatePicker();
		
		picker.addValueChangeHandler(new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				Date date = event.getValue();

				updateTextBox(date);
			}
		});

		this.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {	
				int left = event.getRelativeElement().getAbsoluteLeft();
				int top = event.getRelativeElement().getAbsoluteBottom();
				pickerPopup.setPopupPosition(left, top);

				showDatePicker();
			}
		});

		pickerPopup.add(picker);
		pickerPopup.hide();
	}
	
	private void showDatePicker() {
		String dateStr = getDate();
		if (!dateStr.equals("") && dateStr != null) {
			Date date = DateTimeFormat.getFormat("dd-MM-yyyy")
					.parse(dateStr);
			
			picker.setValue(date);
			picker.setCurrentMonth(date);

		}
		
		pickerPopup.show();
	}
	
	private void updateTextBox(Date date) {
		String dateString = DateTimeFormat.getFormat("dd-MM-yyyy")
				.format(date);
		setDate(dateString);
		pickerPopup.hide();
	}

	public void setDate(String date) {
		//Date dateObj = new Date(date);
		//String dateString = DateTimeFormat.getFormat("dd-MM-yyyy")
		//		.format(dateObj);
		this.setValue(date);
	}
	
	public void setDateToday() {
		Date dateObj = new Date();
		String dateString = DateTimeFormat.getFormat("dd-MM-yyyy")
				.format(dateObj);
		this.setValue(dateString);
	}

	public String getDate() {
		return this.getValue();
	}
	
	public void setWeekStartDate(Date date) {
		int day = date.getDay();
		date.setDate(date.getDate() - day);
		
		picker.setValue(date);

		String dateString = DateTimeFormat.getFormat("dd-MM-yyyy")
				.format(date);
		
		setDate(dateString);
	}
	

	public void setWeekEndDate(Date date) {
		int day = 6 - date.getDay();
		date.setDate(date.getDate() + day);
		
		picker.setValue(date);

		String dateString = DateTimeFormat.getFormat("dd-MM-yyyy")
				.format(date);
		
		setDate(dateString);
	}
}
