package ow.pos.profit.model.component;

import java.util.Comparator;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.cellview.client.TextColumn;

public class TextComparator<T extends JavaScriptObject> implements Comparator<T> {
	private TextColumn<T> column;
	
	public TextComparator(TextColumn<T> col) {
		this.column = col;
		
	}

	@Override
	public int compare(T o1, T o2) {
		if (o1 == o2) {
			return 0;
		}

		if (o1 != null) {
			return (o2 != null) ? column.getValue(o1).compareTo(
					column.getValue(o2)) : 1;
		}

		return -1;
	}
}
