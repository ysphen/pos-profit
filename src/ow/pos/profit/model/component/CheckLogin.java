package ow.pos.profit.model.component;

import ow.pos.profit.shared.ConfigParameters;

import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class CheckLogin {
	public CheckLogin() {
		String url = ConfigParameters.BASEURL
				+ "/jsondataserver/checkisloggedin?";

		JsonpRequestBuilder builder = new JsonpRequestBuilder();

		// send request with null data
		builder.requestString(url, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(String result) {
				if (!result.equals("")) {
					//redirect window
					Window.alert("Session expired. Please log back in.");
			        Window.Location.replace(result); 
				}
			}
		});
	}
}
