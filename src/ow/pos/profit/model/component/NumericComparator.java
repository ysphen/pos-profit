package ow.pos.profit.model.component;

import java.util.Comparator;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.cellview.client.TextColumn;

public class NumericComparator<T extends JavaScriptObject> implements Comparator<T> {
	private TextColumn<T> column;
	
	public NumericComparator(TextColumn<T> col) {
		this.column = col;
		
	}

	@Override
	public int compare(T o1, T o2) {
		if (o1 == o2) {
			return 0;
		}

		if (o1 != null) {
			if (o2 != null) {
				Double o1Dbl = Double.parseDouble(column.getValue(o1));
				Double o2Dbl = Double.parseDouble(column.getValue(o2));

				return o1Dbl.compareTo(o2Dbl);
			} else {
				return -1;
			}
		}

		return -1;
	}
}
