package ow.pos.profit.model.layout;

import java.util.ArrayList;

import ow.pos.profit.shared.ConfigParameters;
import ow.pos.profit.model.structure.BuyerList;
import ow.pos.profit.shared.TableSort;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.ListDataProvider;

public class Buyer extends PopupPanel{

	private CellTable<BuyerList> buyerTable;
	private ListDataProvider<BuyerList> dataProvider;
	private TableSort<BuyerList> tableSorter;
	
	private final int[] numericCols = { 2, 3, 4};
	private final int[] textCols = { 1 };
	
	public Buyer(){
		super(true,true);
		initReport();
		
		this.setStyleName("reportPopup", true);
	}
	
	private void initReport(){
		buyerTable = new CellTable<BuyerList>();
		this.add(buyerTable);
				
		TextColumn<BuyerList> IdCol = new TextColumn<BuyerList>() {
			public String getValue(BuyerList item) {
				return item.getId();
			}
		};
		
		TextColumn<BuyerList> NameCol = new TextColumn<BuyerList>() {
			public String getValue(BuyerList item) {
				return item.getName();
			}
		};
		
		TextColumn<BuyerList> QtyCol = new TextColumn<BuyerList>() {
			public String getValue(BuyerList item) {
				return item.getQty();
			}
		};
		
		TextColumn<BuyerList> PriceCol = new TextColumn<BuyerList>() {
			public String getValue(BuyerList item) {
				return item.getPrice();
			}
		};
		
		TextColumn<BuyerList> TotalCol = new TextColumn<BuyerList>() {
			public String getValue(BuyerList item) {
				return item.getTotal();
			}
		};
		
		buyerTable.addColumn(IdCol, "Id");
		buyerTable.setColumnWidth(IdCol, 60, Unit.PX);
		buyerTable.addColumn(NameCol, "Name");
		buyerTable.setColumnWidth(NameCol, 200, Unit.PX);
		buyerTable.addColumn(QtyCol, "Qty");
		buyerTable.setColumnWidth(QtyCol, 60, Unit.PX);
		buyerTable.addColumn(PriceCol, "Price");
		buyerTable.setColumnWidth(PriceCol, 60, Unit.PX);
		buyerTable.addColumn(TotalCol, "Total");
		buyerTable.setColumnWidth(TotalCol, 60, Unit.PX);
		
		NameCol.setSortable(true);
		QtyCol.setSortable(true);
		PriceCol.setSortable(true);
		TotalCol.setSortable(true);
		
		buyerTable.setTableLayoutFixed(true);
		dataProvider = new ListDataProvider<BuyerList>();
		dataProvider.addDataDisplay(buyerTable);

		tableSorter = new TableSort<BuyerList>(buyerTable, dataProvider);
		
	}
	
	public void showReport(String Id){
		String url = ConfigParameters.BASEURL
		+ "/profitjson/getBuyer?itemId=" + Id;

			JsonpRequestBuilder builder = new JsonpRequestBuilder();
			
			builder.requestObject(url,new AsyncCallback<JsArray<BuyerList>>() {
				
				public void onFailure(Throwable caught) {
				}
				
				public void onSuccess(JsArray<BuyerList> result){
					ArrayList<BuyerList> itmList = new ArrayList<BuyerList>();

					if (result == null) {
						// @TODO do something...
					} else {
						buyerTable.setRowCount(result.length() + 1);
						buyerTable.setPageSize(result.length() + 1);
						
						for(int i = 0; i < result.length(); i++){
							itmList.add(result.get(i));
						}
						
					}
					
					dataProvider.setList(itmList);
					tableSorter.attachSort(numericCols, true);
					tableSorter.attachSort(textCols, false);
					
				}
			});
			
			this.show();
			this.center();
	}
	
}
