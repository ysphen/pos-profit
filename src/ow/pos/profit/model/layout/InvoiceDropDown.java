package ow.pos.profit.model.layout;

import java.util.HashMap;

import ow.pos.profit.model.structure.DropDownItem;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.ui.ListBox;

public class InvoiceDropDown extends ListBox {
	private HashMap<String, Integer> itemMap;

	public InvoiceDropDown() {
	}

	public void setValues(JsArray<DropDownItem> values) {
		this.clear();
		itemMap = new HashMap<String, Integer>();

		for (int i = 0; i < values.length(); i++) {
			DropDownItem item = values.get(i);
			this.addItem(item.getDisplayText(), item.getValue());
			itemMap.put(item.getValue(), i);
		}
		this.setItemSelected(0, true);
	}

	public int getIndexOf(String value) {
		return itemMap.get(value);
	}

	public void setSelectedValue(String value) {
		if (value != null && !value.equals("-1")) {
			this.setSelectedIndex(getIndexOf(value));
		}
	}
}
