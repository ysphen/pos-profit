package ow.pos.profit.shared;

import java.util.ArrayList;

import com.google.gwt.http.client.URL;

public class HTTPGetString {
	
	private ArrayList<String> name;
	private ArrayList<String> value;
	
	public HTTPGetString() {
		name = new ArrayList<String>();
		value = new ArrayList<String>();
	}
	
	public void addParam(String nameStr, String valueStr) {
		name.add(nameStr);
		value.add(valueStr);
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();

		for (int i = 0, n = name.size(); i < n; ++i) {
			if (i > 0) {
				sb.append("&");
			}

			if(name.get(i)!= null && value.get(i) != null){
			// encode the characters in the name
			String encodedName = URL.encodeQueryString(name.get(i));
			sb.append(encodedName);

			sb.append("=");

			// encode the characters in the value
			String encodedValue = URL.encodeQueryString(value.get(i));
			sb.append(encodedValue);
			}
		}

		return sb.toString();
	}
}
