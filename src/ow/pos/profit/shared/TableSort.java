package ow.pos.profit.shared;

import ow.pos.profit.model.component.DateComparator;
import ow.pos.profit.model.component.DateTimeComparator;
import ow.pos.profit.model.component.NumericComparator;
import ow.pos.profit.model.component.TextComparator;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ListDataProvider;

public class TableSort<T extends JavaScriptObject> {
	private CellTable<T> itemTable;
	private ListDataProvider<T> dataProvider;

	public TableSort(CellTable<T> itemTable, ListDataProvider<T> dataProvider) {
		this.itemTable = itemTable;
		this.dataProvider = dataProvider;
	}

	public void attachSort(int cols[], boolean isNumeric) {
		for (int i = 0; i < cols.length; i++) {
			TextColumn<T> column = (TextColumn<T>) itemTable.getColumn(cols[i]);

			ListHandler<T> columnSortHandler = new ListHandler<T>(
					dataProvider.getList());
			if (isNumeric) {
				columnSortHandler.setComparator(column,
						new NumericComparator<T>(column));
			} else {
				columnSortHandler.setComparator(column, new TextComparator<T>(
						column));
			}

			itemTable.addColumnSortHandler(columnSortHandler);

			itemTable.getColumnSortList().push(column);
		}
	}

	public void attachDateSort(int cols, boolean hasTime) {
		TextColumn<T> column = (TextColumn<T>) itemTable.getColumn(cols);

		ListHandler<T> columnSortHandler = new ListHandler<T>(
				dataProvider.getList());
		if (hasTime) {
			columnSortHandler.setComparator(column, new DateTimeComparator<T>(
					column));
		} else {
			columnSortHandler.setComparator(column, new DateComparator<T>(
					column));
		}
		itemTable.addColumnSortHandler(columnSortHandler);

		itemTable.getColumnSortList().push(column);
	}
}
