package ow.pos.profit.shared;

public class ButtonStyles {
	public static final String REGULARBUTTONCLASS = "regularButton";
	public static final String TALLBUTTONCLASS = "tallButton";
	public static final String LARGEBUTTONCLASS = "largeButton";
	public static final String SELECTCUSTOMERBUTTON = "selectCustomerButton";
	public static final String TINYBUTTON = "tinyButton";

}
